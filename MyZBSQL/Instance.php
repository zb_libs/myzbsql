<?php
namespace MyZBSQL;


/**
 * Инстанс базы данных
 * 
 * Можно использовать глобальное подключение:
 * <code>
 * Instance::config($config);
 * $query = new Query('select * from `myzbsql` where `id` = :id;');
 * $query->params(['id' => 1]);
 * $result = $query->execute();
 * var_dump($result->fetch_row());
 * </code>
 * 
 * Или инстанцировать подключение вручную и передавать его в execute-методы:
 * <code>
 * $db = new Instance($config);
 * $query = new Query('select * from `myzbsql` where `id` = :id;');
 * $query->params(['id' => 1]);
 * $result = $query->execute($db);
 * var_dump($result->fetch_row());
 * </code>
 */
class Instance {
    
    private static $instance=false;
    private static $static_config;
    private $config;
    private $connection=false;
    private $error;
    
    public function __construct($options)
    {
        $this->config = $options;
    }
    
    /**
     * @return \MyZBSQL\Instance
     */
    public static function get()
    {
        if (!static::$instance)
        {
            if (!static::$static_config) {
                return false;
            }
            static::$instance = new self(static::$static_config);
        }
        return static::$instance;
    }
    
    public static function config($options)
    {
        if (!static::$instance)
        {
            static::$static_config = $options;
            return true;
        }
        return false;
    }
    
    public function connection()
    {
        if (!$this->connection){
            try {
                $this->connection = @new \mysqli($this->config['host'],$this->config['username'],$this->config['password'],$this->config['dbname']);
                if ($this->connection->connect_error)
                {
                    $this->error = 'no connection '.$this->connection->connect_error;
                }
            } catch (\Exception $e) {
                $this->error = 'no connection '.$e->getMessage();
            }
            
        }
        return $this->connection;
    }
    
    public function error() {
        return $this->error;
    }

    public function close()
    {
        if ($this->connection){
            $this->connection->close();
            $this->connection = false;
        }
    }
}