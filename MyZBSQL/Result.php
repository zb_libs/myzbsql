<?php
namespace MyZBSQL;

if (!defined('MYSQLI_ASSOC')){define('MYSQLI_ASSOC',1);}
if (!defined('MYSQL_NUM')){define('MYSQL_NUM',2);}
if (!defined('MYSQLI_BOTH')){define('MYSQLI_BOTH',3);}

class Result{
    
    private $results = [];
    private $isCollection = false;
    private $errors = false;
    private $result = null;
    private $sql = null;
    private $mysqli_result = null;
    private $raw_sql;
    private $params;
    
    
    public function __construct( $sql, $raw_sql, $params, $mysqli_result, $mysqli_errors )
    {
        $this->sql = $sql;
        $this->raw_sql = $raw_sql;
        $this->params = $params;
        $this->mysqli_result = $mysqli_result;
        $this->errors = $mysqli_errors;
        $this->result = !!$mysqli_result;
    }
    
    public function sql($raw=false){
        return ($raw) ? $this->raw_sql : $this->sql ;
    }
    /**
     * @return mysqli_result 
     */
    public function raw()
    {
        return $this->mysqli_result;
    }
    
    /**
     * разбивает коллекцию результатов на отдельные результаты
     * return \MyZBSQL\Result[] 
     */
    public function collection()
    {
        if ($this->isCollection){
            return $this->results;
        }else{
            return [$this];
        }
    }
    
    /**
     * return bool
     */
    public function result()
    {
        return $this->result;
    }
    
    public function error_list($asString=false)
    {
        if ($asString){
            $string = '';
            foreach ($this->errors as $error){
                $string.=$error['error'].' ';
            }
            return $string;
        }else{
            return $this->errors;
        }
    }
    
    /**
     * 
     */
    public function fetch_all( $type = MYSQLI_NUM, $keyField=null )
    {
        if(!$this->mysqli_result){
            return false;
        }
        $data = [];
        if (!function_exists('mysqli_fetch_all') || $keyField){
            while($row = $this->mysqli_result->fetch_array($type)) {
                if ($keyField){
                    $data[ $row[ $keyField ] ] = $row;
                }else{
                    $data[] = $row;    
                }
            }
        }else{
            $data = $this->mysqli_result->fetch_all($type);       
        }
        return $data;
    }
    
    public function fetch_array( $type = MYSQLI_BOTH )
    {
        switch ($type)
        {
            case MYSQLI_ASSOC:
                return $this->fetch_assoc();
            case MYSQLI_NUM:
                return $this->fetch_row();
            case MYSQLI_BOTH:
                return array_merge($this->fetch_row(),$this->fetch_assoc());
        }
    }
    
    public function fetch_row()
    {
        if (!$this->mysqli_result){
            return false;
        }
        return $this->mysqli_result->fetch_row();
    }
    
    public function fetch_assoc( )
    {
        if (!$this->mysqli_result){
            return false;
        }
        return $this->mysqli_result->fetch_assoc();
    }
    
    public function fetch_object( $class_name = "stdClass", array $params )
    {
        
    }
    
    public function fetch_all_assoc( $keyField = null )
    {
        return $this->fetch_all(MYSQLI_ASSOC, $keyField);    
    }
    
    /**
     * возвращает массив, содержащий в качетсве элементов значения из столбца $field или первого столбца, если не указан $field
     * а в качестве ключей значения из столбца $key, если задано, или номер выбранной строки
     */
    public function fetch_list($field=null,$key=null)
    {
        if (is_null($field)){
            $field = 0;
            $tmpData = $this->fetch_all();
        }else{
            $tmpData = $this->fetch_all_assoc($key);
        }
        $result = [];
        foreach ($tmpData as $id => $row){
            $result[$id]=$row[$field];
        }
        return $result;
    }
    
    /**
     * возвращает значение первого столбца первой выбранной записи
     */
    public function fetch_cell()
    {
        if ($row = $this->fetch_row()){
            return $row[0];
        }
        return false;
    }
    
    /**
     * @see https://php.net/manual/ru/mysqli-result.data-seek.php
     * @param int $offset
     * @return bool
     */
    public function data_seek($offset)
    {
        return $this->raw()->data_seek($offset);
    }
    
    /**
     * Fetch meta-data for a single field
     * @see https://php.net/manual/ru/mysqli-result.fetch-field-direct.php
     * @param int $fieldnr - The field number. This value must be in the range from 0 to number of fields - 1.
     * @return object|false
     */
    public function fetch_field_direct($fieldnr)
    {
        return $this->raw()->fetch_field_direct($fieldnr);
    }
    
    /**
     * Returns the next field in the result set
     * @see https://php.net/manual/ru/mysqli-result.fetch-field.php
     * @return object|false
     */
    public function fetch_field()
    {
        return $this->raw()->fetch_field();
    }
    
    /**
     * @see https://php.net/manual/ru/mysqli-result.fetch-fields.php
     * @return object[]
     */
    public function fetch_fields()
    {
        return $this->raw()->fetch_fields();
    }
    
    /**
     * @see https://php.net/manual/ru/mysqli-result.field-seek.php
     * @param int $fieldnr - The field number. This value must be in the range from 0 to number of fields - 1.
     * @return bool
     */
    public function field_seek( $fieldnr )
    {
        return $this->raw()->field_seek( $fieldnr );
    }
    
    /**
     * 
     * @return void
     */
    public function free()
    {
        return $this->raw()->free();
    }
}
