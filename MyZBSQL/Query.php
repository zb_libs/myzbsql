<?php
namespace MyZBSQL;

class Query{
    
    private $sql;
    private $params;
    private $paramsData = [[]];
    
    public function __construct($sql, array $userdata = null)
    {
        $matches = [];
        preg_match_all('/:([a-zA-Z0-9\-\_]+)/',$sql,$matches);
        $paramsList = [];
        foreach ($matches[1] as $param)
        {
            if (!isset($paramsList[strlen($param)]) || !in_array($param, $paramsList[strlen($param)])){
                $paramsList[strlen($param)][]=$param;
            }
        }
        krsort($paramsList);
        $queryParams = [];
        foreach ($paramsList as $params){
            foreach ($params as $param){
                $queryParams[]=$param;
            }
        }
        $this->sql = $sql;
        $this->params = $queryParams;
        if (!is_null($userdata)){
            $this->params($userdata);
        }
    }
    
    /**
     * установка/изменение параметра запроса
     */
    public function param($name,$value){
        $this->paramsData = array_slice($this->paramsData,0,1,1);
        $this->paramsData[ array_keys($this->paramsData)[0] ][$name] = $value;
    }
    
    /**
     * установка всех параметров запроса
     */ 
    public function params(array $params, $multiply=false)
    {
        if ($multiply && is_array(current($params))){//массив массивов
            $this->paramsData = $params;
        }else{//массив
            $this->paramsData = [ $params ];
        }
    }
    
    public function execute(\MyZBSQL\Instance $dbInstance=null)
    {
        $results = [];
        $sql = str_replace(['[',']'],['(',')'],$this->sql);
        foreach($this->paramsData as $key => $data){
            $result = $this->executeSql($sql, $data, $this->params, $dbInstance);
            $results[$key] = $result;
        }
        return (count($results) === 1)? current($results) : $results;
    }
    
    private function quote($value, $dbInstance){
        if (is_string($value)) {
            $str = $dbInstance->connection()->real_escape_string($value);
            $quoted = "'$str'";
        } elseif (is_int($value)) {
            $quoted = (string) $value;
        } elseif (is_float($value)) {
            $quoted = sprintf('%F', $value);
        } elseif (is_bool($value)) {
            $quoted = $value? 'true' : 'false';
        } elseif (is_null($value)) {
            $quoted = 'null';
        } elseif (is_array($value)) {
            $quotedArr = array();
            foreach ($value as $innerVal) {
		        $str = $this->quote($innerVal, $dbInstance);
				if (is_array($innerVal)) {
					$quotedArr[] = "($str)";
				} else {
					$quotedArr[] = $str;
                }
            }
            $quoted = join(', ', $quotedArr); 
        } else {
            try {
                $str = $dbInstance->connection()->real_escape_string((string)$value);
                $quoted = "'$str'";
            }catch (\Exception $e){
                throw new \Exception('unknown variable type');
            }
        }
        return $quoted;
    }
    
    public function executeAsOne($glue, \MyZBSQL\Instance $dbInstance=null)
    {
        $firstPosition = mb_strpos($this->sql,'[',0,'utf8');
        $length = mb_strpos($this->sql,']',$firstPosition,'utf8') - $firstPosition + 1;
        $shortSql = mb_substr($this->sql, $firstPosition, $length, 'utf8');
        $pattern = str_replace(['[',']'],['(',')'],$shortSql);
        $queries = [];
        foreach($this->paramsData as $data){
            $sql = $pattern;
            foreach($this->params as $paramName){
                $value = (isset($data[$paramName]))? $data[$paramName] : null;
                $quoted = $this->quote($value, $dbInstance);
                $sql = str_replace(':'.$paramName,$quoted,$sql);   
            }
            $queries[] = $sql;
        }
        $sql2 = implode($glue,$queries);
        $resultSql = str_replace($shortSql,$sql2,$this->sql);
        $result = $this->executeSql($resultSql, current($this->paramsData), $this->params, $dbInstance);
        return $result;
    }
    
    private function executeSql($sql, $data, $params, \MyZBSQL\Instance $dbInstance = null) {
        if (is_null($dbInstance)){
            $dbInstance = \MyZBSQL\Instance::get();
        }
        $connection = $dbInstance->connection();
        if ($dbInstance->error()) {
            $result = new \MyZBSQL\Result(
                false,
                $this->sql,
                $data,
                null,
                [[
                    'errno'=>$connection->connect_errno,
                    'sqlstate'=>null,
                    'error'=>$dbInstance->error()
                ]]
            );
            return $result;
        }
        if (count($data) < count($params)){
            $result = new \MyZBSQL\Result(false,$this->sql,$data,null,[['errno'=>0,'sqlstate'=>0,'error'=>'lost parameters']]);
            return $result;
        }
        foreach($params as $paramName){
            $value = (isset($data[$paramName]))? $data[$paramName] : null;
            $quoted = $this->quote($value, $dbInstance);
            $sql = str_replace(':'.$paramName,$quoted,$sql);                
        }
        $mysqli_result = $connection->query($sql);
        $result = new \MyZBSQL\Result($sql,$this->sql,$data,$mysqli_result,$connection->error_list);
        return $result;
    }
    
}
