<?php

namespace MyZBSQL;

class InstanceTest extends \PHPUnit_Framework_TestCase {
    
    protected function tearDown() {
        $instanceClass = new \ReflectionClass('\MyZBSQL\Instance');
        $staticConfig = $instanceClass->getProperty('static_config');
        $staticConfig->setAccessible(true);
        $staticConfig->setValue(null);
        $staticInstance = $instanceClass->getProperty('instance');
        $staticInstance->setAccessible(true);
        $staticInstance->setValue(null);
    }
    
    public function testConfigAndGet() {
        $instance0 = Instance::get();
        $this->assertFalse($instance0);
        
        $result1 = Instance::config([
            'host' => 'localhost',
            'username' => 'root',
            'password' => '',
            'dbname' => 'test',
        ]);
        $this->assertTrue($result1);
        $instance1 = Instance::get();
        $this->assertInstanceOf('\MyZBSQL\Instance', $instance1);
        
        $result2 = Instance::config([
            'host' => 'localhost2',
            'username' => 'root2',
            'password' => '2',
            'dbname' => 'test2',
        ]);
        $this->assertFalse($result2);
    }
    
    public function testConnection() {
        $instance1 = new Instance($GLOBALS['validConfig']);
        $connection1 = $instance1->connection();
        $this->assertInstanceOf('\mysqli', $connection1);
        $this->assertSame(0, $connection1->connect_errno);
        $this->assertEmpty($instance1->error());
        
        $instance2 = new Instance($GLOBALS['invalidConfig']);
        $connection2 = $instance2->connection();
        $this->assertInstanceOf('\mysqli', $connection2);
        $this->assertSame(2002, $connection2->connect_errno);
        $this->assertNotEmpty($instance2->error());
    }
    
}

