<?php

namespace MyZBSQL;

class QueryTest extends \PHPUnit_Framework_TestCase {
    
    private static $db;
    private static $badDb;
    
    public static function setUpBeforeClass() {
        self::$db = new Instance($GLOBALS['validConfig']);
        self::$badDb = new Instance($GLOBALS['invalidConfig']);
    }
    
    public function testBadDbExecute() {
        $query = new Query('drop table if exists `myzbsql`;');
        $result = $query->execute(self::$badDb);
        $this->assertInstanceOf('\MyZBSQL\Result', $result);
        $this->assertFalse($result->result());
    }
    
    public function testBadSqlExecute() {
        $query = new Query('bad sql;');
        $result = $query->execute(self::$db);
        $this->assertInstanceOf('\MyZBSQL\Result', $result);
        $this->assertFalse($result->result());
    }
    
    public function testExecute() {
        $query = new Query('drop table if exists `myzbsql`;');
        $result = $query->execute(self::$db);
        $this->assertInstanceOf('\MyZBSQL\Result', $result);
        $this->assertTrue($result->result());
        
        $query = new Query('
            create table `myzbsql` (
                `id` int unsigned not null auto_increment,
                `text` varchar(255) not null,
                primary key (`id`)
            )
            engine InnoDB;
        ');
        $result = $query->execute(self::$db);
        $this->assertInstanceOf('\MyZBSQL\Result', $result);
        $this->assertTrue($result->result());
    }
    
    /**
     * @depends testExecute
     */
    public function testExecuteAsOne() {
        // один зпрос со множеством параметров
        $query = new Query('insert into `myzbsql` (`text`) values [:text];');
        $query->params([
            ['text' => 'foo'],
            ['text' => 'bar'],
            ['text' => 'baz'],
        ], true);
        $result = $query->executeAsOne(',', self::$db);
        $this->assertInstanceOf('\MyZBSQL\Result', $result);
        
        $query = new Query('select * from `myzbsql`;');
        $result = $query->execute(self::$db);
        $this->assertInstanceOf('\MyZBSQL\Result', $result);
        $this->assertTrue($result->result());
        $this->assertSame([
            ['id' => '1', 'text' => 'foo'],
            ['id' => '2', 'text' => 'bar'],
            ['id' => '3', 'text' => 'baz'],
        ], $result->fetch_all(MYSQLI_ASSOC));
    }
    
    /**
     * @depends testExecuteAsOne
     */
    public function testMultiplyExecute() {
        // несколько запросов (по одному на каждую группу параметров)
        $query = new Query('update `myzbsql` set `text` = :text where `id` = :id;');
        $query->params([
            ['id' => 1, 'text' => 'qux'],
            ['id' => 2, 'text' => 'quux'],
            ['id' => 3, 'text' => 'quuux'],
        ], true);
        $result = $query->execute(self::$db);
        $this->assertContainsOnly('\MyZBSQL\Result', $result);
        
        $query = new Query('select * from `myzbsql`;');
        $result = $query->execute(self::$db);
        $this->assertInstanceOf('\MyZBSQL\Result', $result);
        $this->assertTrue($result->result());
        $this->assertSame([
            ['id' => '1', 'text' => 'qux'],
            ['id' => '2', 'text' => 'quux'],
            ['id' => '3', 'text' => 'quuux'],
        ], $result->fetch_all(MYSQLI_ASSOC));
    }
    
    /**
     * @depends testMultiplyExecute
     */
    public function testParam() {
        $query = new Query('select * from `myzbsql` where `id` = :id;');
        $query->param('id', 1);
        $result = $query->execute(self::$db);
        $this->assertInstanceOf('\MyZBSQL\Result', $result);
        $this->assertTrue($result->result());
        $this->assertSame([
            ['id' => '1', 'text' => 'qux'],
        ], $result->fetch_all(MYSQLI_ASSOC));
    }
    
    /**
     * @depends testMultiplyExecute
     */
    public function testReuseParams() {
        $query = new Query('update `myzbsql` set `text` = :text where `id` = :id;');
        $query->params(['id' => 1, 'text' => 'new']);
        $query->execute(self::$db);
        $selectQuery = new Query('select * from `myzbsql` limit 2;');
        $result = $selectQuery->execute(self::$db);
        $this->assertSame([
            ['id' => '1', 'text' => 'new'],
            ['id' => '2', 'text' => 'quux'],
        ], $result->fetch_all(MYSQLI_ASSOC));
        
        $query->param('id', 2);
        $query->execute(self::$db);
        $selectQuery = new Query('select * from `myzbsql` limit 2;');
        $result = $selectQuery->execute(self::$db);
        $this->assertSame([
            ['id' => '1', 'text' => 'new'],
            ['id' => '2', 'text' => 'new'],
        ], $result->fetch_all(MYSQLI_ASSOC));
    }
    
}
