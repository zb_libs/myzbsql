<?php

$GLOBALS['validConfig'] = [
    'host' => 'localhost',
    'username' => 'root',
    'password' => '123456',
    'dbname' => 'test',
];
$GLOBALS['invalidConfig'] = [
    'host' => 'badhost',
    'username' => 'sws',
    'password' => 'badpassword',
    'dbname' => 'unknowndb',
];

$include = realpath(__DIR__.'/..');
set_include_path(get_include_path().PATH_SEPARATOR.$include);

spl_autoload_register(function($className) {
    $fileName = stream_resolve_include_path(
        strtr(ltrim($className, '\\'), '\\', '/').'.php'
    );

    if ($fileName) {
        require_once $fileName;
    }
});
